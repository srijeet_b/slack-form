const { WebClient, retryPolicies } = require('@slack/web-api');

async function sendMessage(token, messageDetails) {
  try {
    const webClient = new WebClient(token, { retryConfig: retryPolicies.rapidRetryPolicy });
    // console.log(messageDetails);
    const message = await webClient.chat.postMessage({
      channel: messageDetails.channel_id,
      text: messageDetails.text,
      blocks: messageDetails.blocks,
    });
    return message;
  } catch (error) {
    throw new Error(error);
  }
}

async function openModal(
  token,
  triggerID,
  blocks,
  metaData,
  headerText = 'Send Auth Form to Users',
) {
  try {
    const webClient = new WebClient(token);
    const profile = await webClient.views.open({
      trigger_id: triggerID,
      view: {
        type: 'modal',
        title: {
          type: 'plain_text',
          text: headerText,
          emoji: true,
        },
        submit: {
          type: 'plain_text',
          text: 'Submit 📤',
          emoji: true,
        },
        close: {
          type: 'plain_text',
          text: 'Cancel ❌',
          emoji: true,
        },
        external_id: metaData,
        // callback_id: timeStamp,
        blocks,
      },
    });
    return profile;
  } catch (error) {
    throw Error(error);
  }
}

async function openModalCustom(token, triggerID, modalData) {
  try {
    const webClient = new WebClient(token);
    const profile = await webClient.views.open({
      trigger_id: triggerID,
      view: modalData,
    });
    return profile;
  } catch (error) {
    throw Error(error);
  }
}

async function getMembers(token) {
  try {
    const webClient = new WebClient(token, { retryConfig: retryPolicies.rapidRetryPolicy });
    const membersList = await webClient.users.list();
    return membersList;
  } catch (error) {
    throw new Error(error);
  }
}

async function chatUpdate(token, channelID, ts, text, attachments = [], blocks = []) {
  try {
    const webClient = new WebClient(token);
    const updateMessage = await webClient.chat.update({
      token,
      channel: channelID,
      ts,
      text,
      attachments,
      blocks,
    });
    return updateMessage;
  } catch (error) {
    throw new Error(error);
  }
}

async function postPersonally(obj) {
  try {
    // console.log(obj);
    const webClient = new WebClient(obj.token, { retryConfig: retryPolicies.rapidRetryPolicy });
    const postPersonally = await webClient.chat.postEphemeral({
      token: obj.token,
      attachments: obj.attachments,
      channel: obj.channelId,
      text: obj.headText,
      user: obj.user,
    });
    return postPersonally;
  } catch (error) {
    throw Error(error);
  }
}

async function chatDelete(token, channelID, ts) {
  try {
    const webClient = new WebClient(token);
    const deleteMessage = await webClient.chat.delete({ token, channel: channelID, ts });
    return deleteMessage;
  } catch (error) {
    throw new Error(error);
  }
}

async function kickUser(token, channelId, userId) {
  try {
    const webClient = new WebClient(token);
    const deleteMessage = await webClient.conversations.kick({
      token,
      channel: channelId,
      user: userId,
    });
    return deleteMessage;
  } catch (error) {
    throw Error(error);
  }
}
async function conversationJoin(token, channelID) {
  try {
    const webClient = new WebClient(token);
    await webClient.conversations.join({
      token,
      channel: channelID,
    });
    return true;
  } catch (error) {
    throw Error(error);
  }
}
async function getUserConversations(token, userId) {
  try {
    const webClient = new WebClient(token);
    const deleteMessage = await webClient.users.conversations({
      token,
      user: userId,
      types: 'public_channel,private_channel,mpim,im',
    });
    return deleteMessage;
  } catch (error) {
    throw Error(error);
  }
}
async function conversationLeave(token, channelID) {
  try {
    const webClient = new WebClient(token);
    await webClient.conversations.leave({
      token,
      channel: channelID,
    });
    return true;
  } catch (error) {
    throw Error(error);
  }
}
module.exports = {
  postPersonally,
  sendMessage,
  openModal,
  getMembers,
  kickUser,
  conversationLeave,
  conversationJoin,
  chatDelete,
  openModalCustom,
  chatUpdate,
  getUserConversations,
};
