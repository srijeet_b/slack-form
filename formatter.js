const usersformatter = (usersarr) => {
  if (usersarr.members.length) {
    return usersarr.members.reduce((acc, e) => {
      if (!e.is_bot && !e.deleted && e.name !== 'slackbot') {
        acc.push(e.id);
      }
      return acc;
    }, []);
  } else {
    return [];
  }
};

const admindataformatter = (arr) => {
  let o = [
    '*First Name*',
    '*Last Name*',
    '*Tag localtion affiliation*',
    '*Coordinator Name*',
    '*Cell Number*',
    '*Email Address*',
    '*Position*',
    '*Access*',
  ];
  let z = arr.reduce((acc, itm, indx) => {
    acc[o[indx]] = itm instanceof Array ? itm.toString().replace(/,/g, ' , ') : itm;
    return acc;
  }, {});
  let res = '';
  Object.keys(z).forEach((e) => {
    res += `${e}: ${z[e]} \n`;
  });
  return res;
};

const channelformatter = (arr) => {
  if (!arr.channels.length) {
    return [];
  } else {
    return arr.channels.map((e) => e.id);
  }
};

module.exports = {
  usersformatter,
  admindataformatter,
  channelformatter,
};
