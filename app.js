const express = require('express');
const request = require('request');
const dotenv = require('dotenv').config();
const helmet = require('helmet');
const fs = require('fs/promises');
const qs = require('qs');
const https = require('https');
const cors = require('cors');
const { usersformatter, admindataformatter, channelformatter } = require('./formatter');
const {
  sendMessage,
  openModal,
  openModalCustom,
  chatUpdate,
  conversationJoin,
  chatDelete,
  kickUser,
  conversationLeave,
  getMembers,
  postPersonally,
  getUserConversations,
} = require('./slackAPI');
const { restricted, owner, message_ids_processed } = require('./data.json');

const app = express();
app.use(helmet());
app.use(cors());
app.use(require('body-parser').urlencoded({ extended: true }));
app.use(require('body-parser').json());

// app.get('/auth/redirect', async (req, res) => {
//   const errorList = ['invalid_scope_requested', 'access_denied'];
//   if (errorList.includes(req.query.error)) {
//     res.redirect(`${process.env.WEB_HOST}`);
//   }
//   const options = {
//     uri: `https://slack.com/api/oauth.v2.access?code=${req.query.code}&client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}`,
//     method: 'GET',
//   };
//   request(options, async (error, response, body) => {
//     const JSONresponse = JSON.parse(body);
//     if (!JSONresponse.ok) {
//       res
//         .send(`Error encountered: \n${JSON.stringify(JSONresponse)}`)
//         .status(400)
//         .end();
//     } else {
//       JSONresponse.host = process.env.WEB_HOST;
//       console.log(JSONresponse);
//       // const result = await wagner.get('SlackController').slackInstall(JSONresponse);
//       accessTok = JSONresponse.access_token;
//       res.redirect(`${process.env.WEB_HOST}/?success=true&token=${JSONresponse.access_token}`);
//     }
//   });
// });

const tokenVerifier = (event) => {
  const stringified = qs.stringify({ challenge: event.challenge });
  return stringified;
};

app.post('/slack/slashcommands', async (req, res) => {
  await commandsProcessor(req.body);
  return res.status(200).json(req.body.challenge);
});

app.get('/', (req, res) => {
  res.write('Connected');
});

const commandsProcessor = async (body) => {
  // console.log(body);
  if (body.command === '/start') {
    let usersarr =
      body.text !== 'fill' ? [] : usersformatter(await getMembers(process.env.SLACK_BOT_TOKEN));
    await openModal(
      process.env.SLACK_BOT_TOKEN,
      body.trigger_id,
      [
        {
          type: 'input',
          element: {
            type: 'multi_users_select',
            placeholder: {
              type: 'plain_text',
              text: 'Type names to select users',
              emoji: true,
            },
            initial_users: usersarr,
            action_id: 'multi_users_select-action',
          },
          label: {
            type: 'plain_text',
            text: 'Select Users',
            emoji: true,
          },
        },
      ],
      'admin_send_message',
    );
  }
};

app.post('/app/interactivity', async (req, res) => {
  getInteraction(req.body);
  return res.status(200).json();
});

async function getInteraction(bodycontent) {
  try {
    // console.log(bodycontent.payload);
    const content = JSON.parse(bodycontent.payload);
    console.log(content.type);
    if (
      content.type === 'block_actions' &&
      'state' in content &&
      'values' in content.state &&
      Object.values(content.state.values)[0] !== undefined
    ) {
      if (
        Object.values(content.state.values)[0]['static_select-action']['selected_option'][
          'value'
        ].match(/^deleteUser/g)
      ) {
        console.log(
          Object.values(content.state.values)[0]['static_select-action']['selected_option'][
            'value'
          ],
        );
        const usertobedeleted = Object.values(content.state.values)[0]['static_select-action'][
          'selected_option'
        ]['value'].split('-')[1];
        if (usertobedeleted) {
          restricted.push(usertobedeleted);
          await fs.writeFile('./data.json', JSON.stringify({ restricted, owner }));
          chatUpdate(
            process.env.SLACK_BOT_TOKEN,
            content.container.channel_id,
            content.message.ts,
            '*Alert!* :warning:',
            [
              {
                text: `Action was successfull ✅ User <@${usertobedeleted}> Removed`,
              },
            ],
            [],
          );
          // let channels = await getUserConversations(process.env.SLACK_BOT_TOKEN, usertobedeleted);
          // channels = channelformatter(channels);
          // console.log(channels);
          // if (channels.length) {
          //   // await Promise.all(
          //   //   channels.map(async (c) => {
          //   //     await conversationJoin(process.env.SLACK_BOT_TOKEN, c);
          //   //     await kickuser(c, usertobedeleted);
          //   //     await conversationLeave(process.env.SLACK_BOT_TOKEN, c);
          //   //   }),
          //   // );

          // }
        }
      } else {
        chatUpdate(
          process.env.SLACK_BOT_TOKEN,
          content.container.channel_id,
          content.message.ts,
          '*Alert!* :warning:',
          [
            {
              text: 'Action was successfull ✅',
            },
          ],
          [],
        );
      }
    }
    if ('message' in content && content.message.text === 'IMPORTANT :warning:') {
      if (content.actions[0].value.match(/yes/gi)) {
        await openModal(
          process.env.SLACK_BOT_TOKEN,
          content.trigger_id,
          [
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'First Name',
                emoji: true,
              },
            },
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'Last Name',
                emoji: true,
              },
            },
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'TAG location affiliation',
                emoji: true,
              },
            },
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'Coordinator Name',
                emoji: true,
              },
            },
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'Cell Number',
                emoji: true,
              },
            },
            {
              type: 'input',
              element: {
                type: 'plain_text_input',
              },
              label: {
                type: 'plain_text',
                text: 'Email Address',
                emoji: true,
              },
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Position*',
              },
              accessory: {
                type: 'radio_buttons',
                options: [
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'Coordinator',
                    },
                    value: 'coordinator',
                  },
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'Volunteer',
                    },
                    value: 'volunteer',
                  },
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'Staff Member',
                    },
                    value: 'staffMember',
                  },
                ],
                action_id: 'radio_buttons-action',
              },
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Please confirm what you need access to*',
              },
              accessory: {
                type: 'checkboxes',
                options: [
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'Slack',
                    },
                    value: 'slack',
                  },
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'All Filter portals',
                    },
                    value: 'AllFilterPortals',
                  },
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'Wiki',
                    },
                    value: 'wiki',
                  },
                  {
                    text: {
                      type: 'mrkdwn',
                      text: 'TAG CRM',
                    },
                    value: 'tag_crm',
                  },
                ],
                action_id: 'checkboxes-action',
              },
            },
          ],
          'user_response_message',
          'TAG Volunteer Auth.form',
        );
        // chatDelete(process.env.SLACK_BOT_TOKEN, content.container.channel_id, content.message.ts);
        chatUpdate(
          process.env.SLACK_BOT_TOKEN,
          content.container.channel_id,
          content.message.ts,
          'Response received ✅',
          [
            {
              text: 'Message sent successfully ✅',
            },
          ],
          [],
        );
      }
    }
    if (
      'view' in content &&
      'external_id' in content.view &&
      content.view.external_id === 'user_response_message' &&
      content.type === 'view_submission'
    ) {
      const datareceived = Object.values(content.view.state.values).map((e) =>
        typeof Object.values(Object.values(e)[0])[1] === 'string'
          ? Object.values(Object.values(e)[0])[1]
          : Object.values(Object.values(e)[0])[1] instanceof Object &&
            'value' in Object.values(Object.values(e)[0])[1]
          ? Object.values(Object.values(e)[0])[1].value
          : Object.values(Object.values(e)[0])[1].map((f) => f.value),
      );
      // console.log(content);
      // console.log(admindataformatter(datareceived));
      owner.map((owner) => {
        sendMessage(process.env.SLACK_BOT_TOKEN, {
          channel_id: owner,
          text: 'DATA',
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: `Data as Received from <@${content.user.id}> :vibration_mode:`,
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: admindataformatter(datareceived),
              },
            },
            {
              type: 'divider',
            },
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: 'Pick an action from the dropdown list',
              },
              accessory: {
                type: 'static_select',
                placeholder: {
                  type: 'plain_text',
                  text: 'Select an item',
                  emoji: true,
                },
                options: [
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Remove user',
                      emoji: true,
                    },
                    value: `deleteUser-${content.user.id}`,
                  },
                  {
                    text: {
                      type: 'plain_text',
                      text: 'Do nothing!',
                      emoji: true,
                    },
                    value: 'doNothing',
                  },
                ],
                action_id: 'static_select-action',
              },
            },
            {
              type: 'divider',
            },
          ],
        });
      });
    } else if (
      'view' in content &&
      'external_id' in content.view &&
      content.view.external_id === 'admin_send_message'
    ) {
      let users = Object.values(content.view.state.values)[0]['multi_users_select-action'][
        'selected_users'
      ];
      // console.log(users);
      users.map((member) => {
        sendMessage(process.env.SLACK_BOT_TOKEN, {
          channel_id: member,
          text: 'IMPORTANT :warning:',
          blocks: [
            {
              type: 'section',
              text: {
                type: 'mrkdwn',
                text: '*Admin Alert:* Please fill this form',
              },
              accessory: {
                type: 'button',
                text: {
                  type: 'plain_text',
                  text: 'Click here 🖱️',
                  emoji: true,
                },
                value: 'Yes',
                action_id: 'button-action',
              },
            },
          ],
        });
      }),
        postPersonally({
          token: process.env.SLACK_BOT_TOKEN,
          attachments: [
            {
              text: 'Message sent successfully ✅',
            },
          ],
          channelId: content.user.id,
          text: 'Message sent successfully ✅',
          user: content.user.id,
        });
    }
  } catch (e) {
    console.error(e);
  }
}

app.post('/app/events', (req, res) => {
  if (req.body.type === 'url_verification') {
    res.set('Content-Type', 'text/application/x-www-form-urlencoded');
    return res.status(200).send(Buffer.from(tokenVerifier(req.body)));
  }
  eventDirector(req.body);
  return res.status(200);
});

async function eventDirector(events) {
  try {
    switch (events.event.type) {
      // case 'member_joined_channel': {
      //   if (restricted.includes(events.event.user)) {
      //     await kickuser(events.event.channel, events.event.user);
      //   }
      //   break;
      // }
      case 'message': {
        if (
          restricted.includes(events.event.user) &&
          !message_ids_processed.includes(events.event.client_msg_id)
        ) {
          try {
            delay(500);
            await chatDelete(process.env.SLACK_USER_TOKEN, events.event.channel, events.event.ts);
            message_ids_processed.push(events.event.client_msg_id);
            await fs.writeFile(
              './data.json',
              JSON.stringify({ restricted, owner, message_ids_processed }),
            );
          } catch (error) {
            console.error(error);
          }
          // await reqs('/api/chat.delete', { channel: events.event.channel, ts: events.event.ts });
        }
        break;
      }
      default: {
        break;
      }
    }
    return;
  } catch (error) {
    console.error(error);
  }
}

// async function kickuser(channelid, userid) {
//   try {
//     await kickUser(process.env.SLACK_BOT_TOKEN, channelid, userid);
//     return true;
//   } catch (error) {
//     console.error(error);
//   }
// }

function delay(time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time);
  });
}

const port = process.env.PORT || 3001;
app.listen(port, () => {
  console.log(`Running on ${port}`);
});
